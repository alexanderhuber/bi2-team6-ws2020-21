import os

import yaml
import xes

# PyYAML and xes needed.
# In order to install:
# pip install pyyaml
# pip install xes


def set_log_extensions(log, entry):
    log.add_default_extensions()
    log.add_extension(xes.Extension(name="CPEE", prefix="cpee", uri=entry['log']['extension']['cpee']))


def set_log_global_trace_attributes(log, entry):
    log.global_trace_attributes = [
        xes.Attribute(type="string", key="concept:name", value=entry['log']['global']['trace']['concept:name']),
        xes.Attribute(type="string", key="cpee:name", value=entry['log']['global']['trace']['cpee:name'])
    ]


def set_log_global_event_attributes(log, entry):
    log.global_event_attributes = [
        xes.Attribute(type="string", key='concept:instance', value=str(entry['log']['global']['event']['concept:instance'])),
        xes.Attribute(type="string", key='concept:name', value=entry['log']['global']['event']['concept:name']),
        xes.Attribute(type="string", key='concept:endpoint', value=entry['log']['global']['event']['concept:endpoint']),
        xes.Attribute(type="string", key='id:id', value=entry['log']['global']['event']['id:id']),
        xes.Attribute(type="string", key='cpee:uuid', value=entry['log']['global']['event']['cpee:uuid']),
        xes.Attribute(type="string", key='lifecycle:transition',
                      value=entry['log']['global']['event']['lifecycle:transition']),
        xes.Attribute(type="string", key='cpee:lifecycle:transition',
                      value=entry['log']['global']['event']['cpee:lifecycle:transition']),
        xes.Attribute(type="date", key='time:timestamp', value="1970-01-01T00:00:00.000+01:00")
    ]


# def set_log_classifiers(log, entry):
#     log.classifiers = [
#         xes.Classifier(name="Event Name", keys="concept:name"),
#         xes.Classifier(name="Event ID", keys="id:id"),
#         xes.Classifier(name="Event ID Transition", keys="id:id lifecycle:transition"),
#         xes.Classifier(name="CPEE Classifier", keys="concept:name cpee:lifecycle:transition")
#     ]


def trace_set_attributes(trace, entry):
    trace.attributes = [
        xes.Attribute(type="string", key="concept:name", value=entry['log']['trace']['concept:name']),
        xes.Attribute(type="string", key="cpee:name", value=entry['log']['trace']['cpee:name']),
        xes.Attribute(type="string", key="cpee:instance", value=entry['log']['trace']['cpee:instance'])
    ]


def should_log_contain_event(entry):
    transition = entry['event']['cpee:lifecycle:transition']
    considered_transitions = ["activity/calling", "activity/done", "activity/"]
    return transition == 'activity/calling' or transition == 'activity/done'


def set_event_attributes(event, entry):
    event.attributes = [
        xes.Attribute(type="string", key="concept:instance", value=entry['event']['concept:instance']),
        xes.Attribute(type="string", key="concept:name", value=entry['event']['concept:name']),
        xes.Attribute(type="string", key="id:id", value=entry['event']['id:id']),
        xes.Attribute(type="string", key="cpee:instance", value=entry['event']['cpee:instance']),
        xes.Attribute(type="string", key="lifecycle:transition", value=entry['event']['lifecycle:transition']),
        xes.Attribute(type="string", key="cpee:lifecycle:transition", value=entry['event']['cpee:lifecycle:transition']),
        xes.Attribute(type="date", key="time:timestamp", value=entry['event']['time:timestamp'])
    ]


def is_endpoint_available(entry):
    return 'concept:endpoint' in entry['event']


log_directory = os.path.normpath("C:/Users/alexa/Nextcloud/Documents/WS2020-21/Business Intelligence 2/Assignment 1/data/batch09_bus")
log_files = os.listdir(log_directory)

output_directory = os.path.normpath("C:/Users/alexa/Nextcloud/Documents/WS2020-21/Business Intelligence 2/Assignment 1/xes")

file_count = 0

log = xes.Log()
log_properties_specified = False
for file in log_files:
    print(f"Progress: { round(file_count / len(log_files) * 100)} %")
    data = yaml.load_all(open(os.path.join(log_directory, file), 'r'))
    trace = xes.Trace()
    for entry in data:
        if 'log' in entry:
            if not log_properties_specified:
                set_log_extensions(log, entry)
                set_log_global_trace_attributes(log, entry)
                set_log_global_event_attributes(log, entry)
                #set_log_classifiers(log, entry)
                log_properties_specified = True
            trace_set_attributes(trace, entry)
        if 'event' in entry and should_log_contain_event(entry):
            event = xes.Event()
            set_event_attributes(event, entry)
            if is_endpoint_available(entry):
                event.add_attribute(xes.Attribute(type="string", key="cpee:endpoint", value=entry['event']['concept:endpoint']))
            trace.add_event(event)
    log.add_trace(trace)
    file_count += 1


open(os.path.join(output_directory, "xes_logs_" + os.path.basename(log_directory)) + ".xes", 'w').write(str(log))
print("Progress: Finished")


