import os
os.environ["PATH"] += os.pathsep + r"C:/Program Files/Graphviz/bin"

import yaml
import xes

from anytree import Node, RenderTree
from anytree.exporter import JsonExporter, DotExporter

# PyYAML and anytree needed.
# In order to install:
# pip install pyyaml
# pip install anytree


def set_log_extensions(log, entry):
    log.add_default_extensions()
    log.add_extension(xes.Extension(name="CPEE", prefix="cpee", uri=entry['log']['extension']['cpee']))


def set_log_global_trace_attributes(log, entry):
    log.global_trace_attributes = [
        xes.Attribute(type="string", key="concept:name", value=entry['log']['global']['trace']['concept:name']),
        xes.Attribute(type="string", key="cpee:name", value=entry['log']['global']['trace']['cpee:name'])
    ]


def set_log_global_event_attributes(log, entry):
    log.global_event_attributes = [
        xes.Attribute(type="string", key='concept:instance', value=str(entry['log']['global']['event']['concept:instance'])),
        xes.Attribute(type="string", key='concept:name', value=entry['log']['global']['event']['concept:name']),
        xes.Attribute(type="string", key='concept:endpoint', value=entry['log']['global']['event']['concept:endpoint']),
        xes.Attribute(type="string", key='id:id', value=entry['log']['global']['event']['id:id']),
        xes.Attribute(type="string", key='cpee:uuid', value=entry['log']['global']['event']['cpee:uuid']),
        xes.Attribute(type="string", key='lifecycle:transition',
                      value=entry['log']['global']['event']['lifecycle:transition']),
        xes.Attribute(type="string", key='cpee:lifecycle:transition',
                      value=entry['log']['global']['event']['cpee:lifecycle:transition']),
        xes.Attribute(type="date", key='time:timestamp', value="1970-01-01T00:00:00.000+01:00")
    ]


# def set_log_classifiers(log, entry):
#     log.classifiers = [
#         xes.Classifier(name="Event Name", keys="concept:name"),
#         xes.Classifier(name="Event ID", keys="id:id"),
#         xes.Classifier(name="Event ID Transition", keys="id:id lifecycle:transition"),
#         xes.Classifier(name="CPEE Classifier", keys="concept:name cpee:lifecycle:transition")
#     ]


def trace_set_attributes(trace, entry):
    trace.attributes = [
        xes.Attribute(type="string", key="concept:name", value=entry['log']['trace']['concept:name']),
        xes.Attribute(type="string", key="cpee:name", value=entry['log']['trace']['cpee:name']),
        xes.Attribute(type="string", key="cpee:instance", value=entry['log']['trace']['cpee:instance'])
    ]


def should_log_contain_event(entry):
    transition = entry['event']['cpee:lifecycle:transition']
    considered_transitions = ["activity/calling", "activity/done", "task/instantiation"]
    return transition in considered_transitions
    #return transition == 'activity/calling' or transition == 'activity/done'


def set_event_attributes(event, entry):
    event.attributes = [
        xes.Attribute(type="string", key="concept:instance", value=entry['event']['concept:instance']),
        xes.Attribute(type="string", key="concept:name", value=entry['event']['concept:name']),
        xes.Attribute(type="string", key="id:id", value=entry['event']['id:id']),
        xes.Attribute(type="string", key="cpee:instance", value=entry['event']['cpee:instance']),
        xes.Attribute(type="string", key="lifecycle:transition", value=entry['event']['lifecycle:transition']),
        xes.Attribute(type="string", key="cpee:lifecycle:transition", value=entry['event']['cpee:lifecycle:transition']),
        xes.Attribute(type="date", key="time:timestamp", value=entry['event']['time:timestamp'])
    ]


def is_endpoint_available(entry):
    return 'concept:endpoint' in entry['event']


def spawns_subprocess(entry):
    transition = entry['event']['cpee:lifecycle:transition']
    return transition == 'task/instantiation'


log_directory = os.path.normpath("C:/Users/alexa/Nextcloud/Documents/WS2020-21/Business Intelligence 2/Assignment 1/data/batch08_bus")
log_files = os.listdir(log_directory)

output_directory = os.path.normpath("C:/Users/alexa/Nextcloud/Documents/WS2020-21/Business Intelligence 2/Assignment 1/xes")

file_count = 0

root = Node(os.path.basename(log_directory))
nodes = []
sub_processes = []

for file in log_files:
    print(f"Progress: {round(file_count / len(log_files) * 100)} %")
    # if file_count > 100:
    #     break
    node_exists = False
    if not file in sub_processes:
        node = Node(file[:-9]) # removing file extension .xes.yaml
    else:
        node_exists = True
        for entry in nodes:
            if entry.name == file[:-9]:
                node = entry
                break
    data = yaml.load_all(open(os.path.join(log_directory, file), 'r'))
    for entry in data:
        if 'event' in entry and spawns_subprocess(entry):
            sub_process = entry['event']['data']['data_receiver']['CPEE-INSTANCE-UUID']
            sub_processes.append(sub_process)
            child = Node(sub_process, parent=node)
            nodes.append(child)
    if not node_exists:
        nodes.append(node)
    file_count += 1

for node in nodes:
    if node.parent is None:
        node.parent = root

#open(os.path.join(output_directory, "xes_logs_" + os.path.basename(log_directory)) + ".xes", 'w').write(str(log))

exporter = JsonExporter(indent=2, sort_keys=True)
with open(os.path.join(output_directory, "log_structure_" + os.path.basename(log_directory) + ".json"),'w') as file:
    exporter.write(root, file)
DotExporter(root).to_picture(os.path.join(output_directory, os.path.basename(log_directory) + "_tree.svg"))

print("Progress: Finished")


