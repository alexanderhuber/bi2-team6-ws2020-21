import os

import yaml
import json

collection = []
batches = ['batch07_bus', 'batch08_bus', 'batch09_bus']
output_directory = os.path.normpath(
    "C:/Users/alexa/Nextcloud/Documents/WS2020-21/Business Intelligence 2/bi2-team6-ws2020-21/Assignment 1/loganalysis")

for batch in batches:

    log_directory = os.path.normpath("C:/Users/alexa/Nextcloud/Documents/WS2020-21/Business Intelligence 2/Assignment 1/data/" + batch)
    log_files = os.listdir(log_directory)

    file_count = 0

    for file in log_files:
        print(f"Progress: {round(file_count / len(log_files) * 100)} %")
        # if file_count > 10:
        #     break
        filename = file[:-9]
        processname = ""
        data = yaml.load_all(open(os.path.join(log_directory, file), 'r'))
        for entry in data:
            if 'log' in entry:
                processname = entry['log']['trace']['cpee:name']
                break
        fileinfo = {'filename': file[:-9], 'processname': processname}
        collection.append(fileinfo)
        file_count += 1


json = json.dumps(collection)
with open(os.path.join(output_directory, 'process_instance_log_relation_' + 'all_batches' + ".json"), 'w') as file:
    file.write(json)
print("Progress: Finished")


