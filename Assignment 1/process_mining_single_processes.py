# pip install python-graphviz
# pip install pm4py

import os
os.environ["PATH"] += os.pathsep + r"C:/Program Files/Graphviz/bin"

import pm4py
from pm4py.objects.log.importer.xes import importer as xes_importer
from pm4py.visualization.petrinet import visualizer as pn_visualizer
from pm4py.algo.discovery.alpha import algorithm as alpha_miner

log_directory = os.path.normpath("C:/Users/alexa/Nextcloud/Documents/WS2020-21/Business Intelligence 2/bi2-team6-ws2020-21/Assignment 1/xes/single_processes_without_spawns")
log_files = os.listdir(log_directory)

for file in log_files:

    log = xes_importer.apply(os.path.join(log_directory, file))

    #net, initial_marking, final_marking = alpha_miner.apply(log)
    #gviz = pn_visualizer.apply(net, initial_marking, final_marking)
    #pn_visualizer.view(gviz)

    net, initial_marking, final_marking = pm4py.discover_petri_net_inductive(log, 0.1)
    #pm4py.view_petri_net(net, initial_marking, final_marking, format="svg")


    from pm4pybpmn.objects.conversion.petri_to_bpmn import factory as bpmn_converter
    bpmn_graph, elements_correspondence, inv_elements_correspondence, el_corr_keys_map = bpmn_converter.apply(net, initial_marking, final_marking)

    # from pm4pybpmn.objects.bpmn.util import bpmn_diagram_layouter
    # bpmn_graph = bpmn_diagram_layouter.apply(bpmn_graph)

    from pm4pybpmn.visualization.bpmn import factory as bpmn_vis_factory
    bpmn_figure = bpmn_vis_factory.apply(bpmn_graph, variant="alignments", parameters={"format" : "svg"})
    #bpmn_vis_factory.view(bpmn_figure)


    bpmn_vis_factory.save(bpmn_figure, os.path.join("processmodels", "single_processes", file[4:] + ".svg"))

    from pm4py.algo.conformance.tokenreplay import algorithm as token_replay
    replayed_traces = token_replay.apply(log, net, initial_marking, final_marking)
    for trace in replayed_traces:
        print("Is Fit: " + str(trace.get('trace_is_fit')) + ", Trace Fitness: " + str(trace.get('trace_fitness')))
