import altair as alt
alt.data_transformers.disable_max_rows()


class DataPlotter:

    # Common plot method
    def plot_facet(self, df, x, y, group_by, n_cols=3, title="", subtitle="", plot_width=600, plot_height=300):
        data = df.copy()
        data = self.get_altair_df(data)
        #x_format = ' [s]' if x == 'timestamp' else ''
        #y_format = ' [mm]' if y == 'value' else ''
        plot = alt.Chart(data).mark_line().encode(
            x=alt.X(x, axis=alt.Axis(title=x.capitalize())),
            y=alt.Y(y, axis=alt.Axis(title=y.capitalize())),
            facet=alt.Facet(group_by + ':N', columns=n_cols)
        ).properties(
            title={
              "text": "Input data", 
              "subtitle": [title, subtitle],
            },
            width=plot_width,
            height=plot_height
        ).configure(background='transparent').configure_title(
            fontSize=20,
            align='center',
            anchor='middle'
        )
        return plot

    def plot_hist(self, data, bin_variable, plot_width=300, plot_height=150):
        plot = alt.Chart(data).mark_bar().encode(
            x=alt.X(bin_variable, bin=True),
            y='count()'
        ).properties(
            width=plot_width,
            height=plot_height
        )
        return plot

    # Helper method to change timedelta columns to total seconds (float), as altair can't deal with timedeltas.
    def get_altair_df(self, df):
        td_cols = list(df.select_dtypes(include='timedelta64').columns)
        for col in td_cols:
            df[col] = df[col].dt.total_seconds()
        df = df.astype({'batch': str})
        return df