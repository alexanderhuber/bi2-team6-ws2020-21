import os
import pm4py
from pm4py.objects.log.importer.xes import importer as xes_importer
from pm4py.visualization.petrinet import visualizer as pn_visualizer
from pm4py.algo.discovery.alpha import algorithm as alpha_miner
from pm4py.algo.discovery.heuristics import algorithm as heuristics_miner
from pm4py.evaluation.replay_fitness import evaluator as replay_fitness_evaluator


class LogMiner:

    def __init__(self, batch_no=0):
        if batch_no == 0:
            self.batch = "all_batches"
        else:
            self.batch = "batch0" + str(batch_no) + "_bus"
        self.read_log()

    def change_batch(self, batch_no):
        if batch_no == 0:
            self.batch = "all_batches"
        else:
            self.batch = "batch0" + str(batch_no) + "_bus"
        self.read_log()

    def read_log(self, path=None):
        if path is not None:
            self.log = xes_importer.apply(path)
        else:
            self.log = xes_importer.apply(os.path.join("xes", "xes_log_with_subprocesses_" + self.batch + ".xes"))
        return self.log

    def apply_alpha_miner(self, display=False, save_bpmn=False):
        net, initial_marking, final_marking = alpha_miner.apply(self.log)

        if display:
            gviz = pn_visualizer.apply(net, initial_marking, final_marking)
            pn_visualizer.view(gviz)
        if save_bpmn:
            self.petrinet_to_bpmn(self, net, initial_marking, final_marking)

        fitness_token = self.conformance_check(self.log, net, initial_marking, final_marking)
        return net, fitness_token

    def apply_heuristics_miner(self, display=False, save_bpmn=False):

        net, initial_marking, final_marking = heuristics_miner.apply(self.log, parameters={
            heuristics_miner.Variants.CLASSIC.value.Parameters.DEPENDENCY_THRESH: 0.99})

        if display:
            gviz = pn_visualizer.apply(net, initial_marking, final_marking)
            pn_visualizer.view(gviz)
        if save_bpmn:
            self.petrinet_to_bpmn(self, net, initial_marking, final_marking)

        fitness_token = self.conformance_check(self.log, net, initial_marking, final_marking)
        return net, fitness_token
    
    def apply_inductive_miner(self, display=False, save_bpmn=False):
        net, initial_marking, final_marking = pm4py.discover_petri_net_inductive(self.log)

        if display:
            pm4py.view_petri_net(net, initial_marking, final_marking, format="png")
        if save_bpmn:
            self.petrinet_to_bpmn(self, net, initial_marking, final_marking)

        fitness_token = self.conformance_check(self.log, net, initial_marking, final_marking)
        return net, fitness_token

    def conformance_check(self, log, net, initial_marking, final_marking, variant=replay_fitness_evaluator.Variants.TOKEN_BASED):
        fitness_token = replay_fitness_evaluator.apply(log, net, initial_marking, final_marking,
                                                       variant=variant)
        return fitness_token


    def petrinet_to_bpmn(self, net, initial_marking, final_marking):
        from pm4pybpmn.objects.conversion.petri_to_bpmn import factory as bpmn_converter
        from pm4pybpmn.visualization.bpmn import factory as bpmn_vis_factory

        bpmn_graph, elements_correspondence, inv_elements_correspondence, el_corr_keys_map = bpmn_converter.apply(net,
                                                                                                                  initial_marking,
                                                                                                                  final_marking)

        # from pm4pybpmn.objects.bpmn.util import bpmn_diagram_layouter
        # bpmn_graph = bpmn_diagram_layouter.apply(bpmn_graph)

        bpmn_figure = bpmn_vis_factory.apply(bpmn_graph, variant="alignments", parameters={"format": "svg"})
        # bpmn_vis_factory.view(bpmn_figure)

        bpmn_vis_factory.save(bpmn_figure, os.path.join("processmodels", "processmodel_" + self.batch + ".svg"))


m = LogMiner()

print(m.apply_alpha_miner(display=True))
print(m.apply_heuristics_miner(display=True))
print(m.apply_inductive_miner(display=True))