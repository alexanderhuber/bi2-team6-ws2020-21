from database.db_manager import DatabaseManager
from process_YAML import Log
import os
from tqdm import tqdm
import pandas as pd


class DataManagement:

    def __init__(self):
        self.reader = Log()
        self.db = DatabaseManager()

    def import_data(self, directory, batch):
        self.import_filenames(directory, batch)
        self.import_relations(directory, batch)
        self.import_keyence_measurements(directory, batch)
        self.import_microvu_success(directory, batch)
        self.import_microvu_upright(directory, batch)

    # save file names together with the process name (table: file_concepts)
    def import_filenames(self, directory, batch):
        print("Importing file names")
        for file in tqdm(os.listdir(directory)):
            path = directory+ '/' +file
            header = self.reader.get_elements(path, getHeaderOnly=True)

            try:
                trace = header['log']['trace']
                concept_name = trace['concept:name']
                cpee_name = trace['cpee:name']
                cpee_instance = trace['cpee:instance']

                sql = " INSERT INTO file_concepts VALUES ('{}', '{}', '{}', '{}'); ".format(
                    file, concept_name, cpee_name, batch)
                self.db.exec_sql(sql)

            except Exception as e:
                print("Exception at file ", file, ": ", e)

    # save parent and child file relations (table: file_relations)
    def import_relations(self, directory, batch):
        print("Importing relations")
        for file in tqdm(os.listdir(directory)):
            path = directory + '/' + file
            header, body = self.reader.get_elements(path, returnSeparate=True)
            try:
                trace = header[0]['log']['trace']
                parent = trace['cpee:instance']

                for element in body:
                    event = element['event']
                    try:
                        if event['cpee:lifecycle:transition'] == 'task/instantiation':
                            file_id = event['data']['data_receiver']['CPEE-INSTANCE-UUID']

                            sql = " INSERT INTO file_relations VALUES ('{}', '{}', '{}'); ".format(
                                file_id, parent, batch)
                            self.db.exec_sql(sql)

                    except Exception:
                        print("Exception for event in file ", file)
            except Exception as e:
                print("Exception for file header ", file)

    # get measurements (table: keyence_measurements)
    def import_keyence_measurements(self, directory, batch):
        # Reading files
        all_files = []
        keys = ['log', 'trace', 'cpee:name']
        target = ["Turm Keyence Measuring"]
        files = self.reader.get_files_with_value(directory=directory, dict_path=keys, value=target, header_only=True)
        all_files.extend(files)

        all_logs = []
        print("Importing keyence measurements")
        print(all_files)
        for file in tqdm(all_files):
            log = self.reader.get_elements(file)

            # Getting measurements only
            values = {}
            try:
                trace_name = log[0]['log']['trace']['concept:name']
                trace_instance = log[0]['log']['trace']['cpee:instance']

            except Exception as e:
                print("header")
                print(e)

            measurements = []
            i = 0
            for event in log:
                try:
                    if event['event']['concept:name'] == 'Fetch' \
                            and event['event']['cpee:lifecycle:transition'] == 'activity/receiving':

                        data_list = event['event']['data']['data_receiver'][0]['data']
                        if data_list is not None and len(data_list) > 0:
                            for data in data_list:
                                sql = " INSERT INTO keyence_measurements VALUES ('{}', '{}', '{}', '{}', {}, '{}', '{}'); ".format(
                                    trace_name, trace_instance, data['value'], data['name'], i, data['timestamp'], batch)
                                #print(sql)
                                self.db.exec_sql(sql)
                                i = i+1
                except Exception as e:
                    pass

    # get microvu success (table: microvu_measurements)
    def import_microvu_success(self, directory, batch):
        all_files = []
        keys = ['log', 'trace', 'cpee:name']
        target=['Spawn Turm Production']
        files = self.reader.get_files_with_value(directory=directory, dict_path=keys, value=target, header_only=True)
        all_files.extend(files)

        all_logs = []
        for file in tqdm(all_files):
            log = self.reader.get_elements(file)
            values = {}

            try:
                trace_name = log[0]['log']['trace']['concept:name']
                trace_instance = log[0]['log']['trace']['cpee:instance']

            except Exception as e:
                print("header")
                print(e)

            measurements = []
            i = 0
            for event in log:
                try:

                    if event['event']['concept:name'] == 'Measure with MicroVu - Lying' \
                         and event['event']['cpee:lifecycle:transition'] == 'dataelements/change':

                        data = event['event']['data']['data_values']

                        upright = data['qc2_upright_success']
                        lying =  data['qc2_lying_success']

                        sql = " INSERT INTO microvu_measurements VALUES ('{}', '{}', '{}', '{}', '{}', '{}'); ".format(
                            trace_name, trace_instance, upright, lying, "", batch)
                        #print(sql)
                        self.db.exec_sql(sql)


                except Exception as e:
                    pass

    def import_microvu_upright(self, directory, batch):
        all_files = []
        keys = ['log', 'trace', 'cpee:name']
        target = ['Spawn Turm Production']
        files = self.reader.get_files_with_value(directory=directory, dict_path=keys, value=target, header_only=True)
        all_files.extend(files)

        all_logs = []
        for file in tqdm(all_files):
            log = self.reader.get_elements(file)
            values = {}

            try:
                trace_name = log[0]['log']['trace']['concept:name']
                trace_instance = log[0]['log']['trace']['cpee:instance']

            except Exception as e:
                print("header")
                print(e)

            measurements = []
            i = 0
            for event in log:
                try:

                    if event['event']['concept:name'] == 'Measure with MicroVu - Upright' \
                            and event['event']['cpee:lifecycle:transition'] == 'dataelements/change':
                        data = event['event']['data']['data_values']

                        upright = data['qc2_upright_success']

                        sql = " INSERT INTO microvu_measurements_flag VALUES ('{}', '{}', '{}', '{}', '{}', '{}'); ".format(
                            trace_name, trace_instance, upright, "", "", batch)
                        # print(sql)
                        self.db.exec_sql(sql)


                except Exception as e:
                    pass

    # returns a dataframe with the measurements
    def get_measurements(self):
        df = pd.read_sql("select * from keyence_measurements", con=self.db.conn)
        return df

    # general query
    def db_query(self, sql):
        df = pd.read_sql(sql, con=self.db.conn)
        return df



