import os
os.environ["PATH"] += os.pathsep + r"C:/Program Files/Graphviz/bin"

import yaml
import xes

from anytree import Node, AnyNode, RenderTree
from anytree.exporter import JsonExporter, DotExporter

# PyYAML and anytree needed.
# In order to install:
# pip install pyyaml
# pip install anytree

log_directory = os.path.normpath("C:/Users/alexa/Nextcloud/Documents/WS2020-21/Business Intelligence 2/Assignment 1/data/batch09_bus")
log_files = os.listdir(log_directory)
root = AnyNode(id=os.path.basename(log_directory))
root.processname = os.path.basename(log_directory)
nodes = []

def spawns_subprocess(entry):
    transition = entry['event']['cpee:lifecycle:transition']
    return transition == 'task/instantiation'


def get_node(name):
    for node in nodes:
        if node.id == name:
            return node
    node = AnyNode(id=name)
    nodes.append(node)
    return node


output_directory = os.path.normpath("C:/Users/alexa/Nextcloud/Documents/WS2020-21/Business Intelligence 2/bi2-team6-ws2020-21/Assignment 1/loganalysis")

file_count = 0

for file in log_files:
    print(f"Progress: {round(file_count / len(log_files) * 100)} %")
    # if file_count > 10:
    #     break
    file_name = file[:-9] # removing file extension .xes.yaml
    node = get_node(file_name)
    data = yaml.load_all(open(os.path.join(log_directory, file), 'r'))
    for entry in data:
        if 'log' in entry:
            node.processname = entry['log']['trace']['cpee:name']
        if 'event' in entry and spawns_subprocess(entry):
            sub_process_uuid = entry['event']['data']['data_receiver']['CPEE-INSTANCE-UUID']
            child = get_node(sub_process_uuid)
            child.parent = node
    file_count += 1

for node in nodes:
    if node.parent is None:
        node.parent = root

exporter = JsonExporter(indent=2, sort_keys=True)
with open(os.path.join(output_directory, "log_structure_with_processnames_" + os.path.basename(log_directory) + ".json"),'w') as file:
    exporter.write(root, file)
DotExporter(root, nodenamefunc=lambda n: '%s' % (n.processname)).to_picture(os.path.join(output_directory, os.path.basename(log_directory) + "_processnames_tree.svg"))

print("Progress: Finished")


