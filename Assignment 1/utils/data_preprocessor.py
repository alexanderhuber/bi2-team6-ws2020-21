import pandas as pd
import numpy as np
from scipy.signal import wiener, filtfilt, butter, gaussian, freqz
from scipy.ndimage import filters
from statsmodels.tsa.api import ExponentialSmoothing, SimpleExpSmoothing
import warnings


class DataPreprocessor:

    def __init__(self, data):
        self.raw = data
        self.data = data

    def normalize_scan_times(self, df, scale):
        # Normalize timestamps to zero
        df.timestamp = df.timestamp - df.groupby('instance')['timestamp'].transform('min')
        scaled_time = scale / df.groupby('instance')['timestamp'].transform('max').dt.total_seconds()
        new_times = df.timestamp * scaled_time
        new_times = new_times.round('10L')
        df.timestamp = new_times
        return df

    def resample(self, data, freq):
        cols = list(data.columns)
        agg_dict = {x: 'first' if x != 'value' else 'mean' for x in cols}
        resampled_data = data.groupby(pd.Grouper(key='timestamp', freq=freq)).agg(agg_dict)
        index = resampled_data.index.name
        resampled_data = resampled_data.drop(index, axis=1)
        resampled_data = resampled_data.pad()
        return resampled_data

    def get_processed_data(self, time_span=100, sample_freq='100L'):
        # Filter out ledges
        data = self.data[self.data['value'] < 999]
        # Normalize scannings
        data = self.normalize_scan_times(data, time_span)
        # Resample data
        instance_data = data.groupby('name', sort=False)
        resampled_data = instance_data.apply(lambda x: self.resample(x, sample_freq))
        data = resampled_data.drop('name', axis=1).reset_index()
        # Fix reversed series
        data = self.__correct_reversed_series(data)
        # Remove initial spikes for batches 10 and 11
        data = self.__remove_initial_spikes(data, sample_freq)
        # Smooth steep steps
        #data = self.__smooth_steep_diam_changes(data, 1.35, 6, time_span, sample_freq)
        # General smoothing
        #data = self.__smooth_series(data)
        return data

    def set_batches(self, batches):
        self.data = self.raw[self.raw['batch'].isin(batches)]

    def get_correlations(self, data, reverse=False):
        instance_data = data.groupby('name', sort=False)['value']
        instance_data = instance_data.apply(lambda x: pd.Series(x.values)).unstack()
        # Get median from observations
        median = instance_data.median(axis=0)
        if reverse:
            # Reverse measurement data
            instance_data = instance_data.apply(self.__reverse_row_values, axis=1)
        # Get correlations
        corr = instance_data.corrwith(median, axis=1)
        return pd.Series(corr, index=instance_data.index)

    def __correct_reversed_series(self, data):
        corr = self.get_correlations(data)
        corr_reversed = self.get_correlations(data, reverse=True)
        df = pd.concat([corr, corr_reversed], axis=1)
        # Define wrong series based on correlation values / parameters probably need to be tuned 
        no_good_orig_correlation = df[0] < 0.9
        better_reversed_correlation = df[1] > 0.9
        series_to_reverse = df.where(no_good_orig_correlation & better_reversed_correlation).dropna()
        names = series_to_reverse.index.to_list()
        warnings.warn(f"The following series got reversed, as I assumed it was reversed perhaps: {str(names)}")
        instance_data = data.groupby('name', sort=False)
        return instance_data.apply(lambda x: self.__reorder(x, names))

    def __reverse_row_values(self, x):
        idx = x.index
        return pd.Series(x.iloc[::-1].values, index=idx)

    def __reorder(self, x, names):
        if x.name in names:
            x['value'] = x['value'].iloc[::-1].values
        return x

    def __remove_initial_spikes(self, df, sample_freq):
        instance_data = df.groupby('name', sort=False)
        df = instance_data.apply(self.__remove_spike_from_single_series).drop(['name'], axis=1).reset_index()
        df = df.drop('level_1', axis=1)
        df = self.normalize_scan_times(df, 100)
        # Resample data
        instance_data = df.groupby('name', sort=False)
        resampled_data = instance_data.apply(lambda x: self.resample(x, sample_freq))
        df = resampled_data.drop('name', axis=1).reset_index()
        return df
    
    def __remove_spike_from_single_series(self, df):
        ## Removal of initial spikes - seconds set manually here: Trial end error
        df = df.where((df['batch'] < 10) | (df['timestamp'].dt.total_seconds() > 3)).dropna()
        #df = df.where((df['batch'] > 9) | (df['timestamp'].dt.total_seconds() > 2)).dropna()
        first_index = df.index[0]
        first_good_index = df.loc[df['value'] < 22.1].index[0]
        first_good_value = df.at[first_good_index, 'value']
        df.at[first_index:first_good_index, 'value'] = first_good_value
        return df
    
    def __smooth_steep_diam_changes(self, df, interval, threshold, time_span, sample_freq):
        instance_data = df.groupby('name', sort=False)
        df = instance_data.apply(lambda x: self.__smooth_single_instance(x, interval, threshold))
        return df
        
    def __smooth_single_instance(self, df, interval, threshold):
        data_points = len(df)
        points_per_mm = data_points / 50
        interval = int(np.ceil(points_per_mm * interval * 0.5))
        value_series = pd.Series(df['value'].values)
        before_x = value_series.shift(periods=interval).values
        after_x = value_series.shift(periods=-interval).values
        deltas = [neg - pos for (neg, pos) in zip(before_x, after_x)]
        #print(deltas)
        values = [after if abs(before - after) > (threshold * 0.7) else x for (x, before, after) in zip(value_series, before_x, after_x)]
        df['value'] = values
        return df
    
    def __smooth_series(self, data):
        instance_data = data.groupby('instance', sort=False)
        return instance_data.apply(self.__smooth)
        
    def __smooth(self, instance):
        #b = gaussian(39, 1)
        b = gaussian(20, 1.2)
        ga = filters.convolve1d(instance['value'], b/b.sum())
        instance['smooth'] = ga
        return instance
        

        