from process_YAML import Log
from log_to_db import DatabaseManager
import pandas as pd

class QualityPrediction:

    def __init__(self):
        self.folders = ['data/batch07_bus/', 'data/batch08_bus/', 'data/batch09_bus/']
        self.db_manager = DatabaseManager()
        self.db_manager.create_connection()
        self.db_manager.create_tables()
        self.log_reader = Log()

    def get_file_names(self, target = ["Turm Keyence Measuring"]):
        all_files = []

        for folder in self.folders:
            # Each file contains the measurements for one piece
            # Getting all measurements files
            keys = ['log', 'trace', 'cpee:name']
            files = self.log_reader.get_files_with_value(directory=folder, dict_path=keys, value=target, header_only=True)
            all_files.extend(files)  # filenames
        return all_files


    def get_keyence_data(self):
        # Reading files
        all_files = self.get_file_names()

        all_logs = []
        for file in all_files:
            log = self.log_reader.get_elements(file)

            #all_logs.append(elements_dict)

            # Getting measurements only
            values = {}
            try:
                trace_name = log[0]['log']['trace']['concept:name']
                trace_instance = log[0]['log']['trace']['cpee:instance']

            except Exception as e:
                print("header")
                print(e)


            measurements = []
            i = 0
            for event in log:
                try:
                    if event['event']['concept:name'] == 'Fetch' \
                            and event['event']['cpee:lifecycle:transition'] == 'activity/receiving':

                        data_list = event['event']['data']['data_receiver'][0]['data']
                        if data_list is not None and len(data_list) > 0:
                            for data in data_list:
                                self.db_manager.insert_measurements(name=trace_name, instance=trace_instance,
                                        value=data['value'], type=data['name'] , log_order=i, timestamp=data['timestamp'], batch=file)
                                i = i+1
                except:
                    pass

    def microvu(self):

        all_files = self.get_file_names(target=['Spawn Turm Production'])
        all_logs = []
        for file in all_files:
            log = self.log_reader.get_elements(file)
            values = {}

            try:
                trace_name = log[0]['log']['trace']['concept:name']
                trace_instance = log[0]['log']['trace']['cpee:instance']

            except Exception as e:
                print("header")
                print(e)

            measurements = []
            i = 0
            for event in log:
                try:

                    if event['event']['concept:name'] == 'Measure with MicroVu - Lying' \
                         and event['event']['cpee:lifecycle:transition'] == 'dataelements/change':

                        data = event['event']['data']['data_values']

                        upright = data['qc2_upright_success']
                        lying =  data['qc2_lying_success']

                        self.db_manager.insert_microvu_measurements(name=trace_name, instance=trace_instance,
                                upright_value=upright,
                                lying_value=lying, info="", batch=file)

                except:
                    pass


    # read data to pandas dataframe
    def read_values(self):
        df = pd.read_sql("select * from measurements_microvu", con=self.db_manager.conn)
        #print(df)
        return df
    """
    def load_parent(self, folder, batch):

        # from anytree.importer import JsonImporter
        # from anytree.search import findall
        importer = JsonImporter()
        with open(folder, 'r') as file:
            root = importer.import_(file.read())

        parent_processes = []
        keyence_processes = []
        parents = findall(root, filter_=lambda node: node.processname == "Spawn Turm Production")
        for parent in parents:
            parent_processes.append(parent.id)
            child = findall(parent, filter_=lambda node: node.processname == "Turm Keyence Measuring")[0]
            keyence_processes.append(child.id)

        lookup = pd.DataFrame(list(zip(parent_processes, keyence_processes)), columns=['Parent', 'Keyence'])
        lookup['Batch'] = batch
        print(lookup)

        lookup.to_sql('parent_map', con=self.db_manager.conn, if_exists='append')
        #self.db_manager.exec_sql("SELECT * FROM TableMap")
    """


#p = QualityPrediction()
#p.load_parent(folder="loganalysis/log_structure_with_processnames_batch07_bus.json", batch=7)
#p.load_parent(folder="loganalysis/log_structure_with_processnames_batch08_bus.json", batch=8)
#p.load_parent(folder="loganalysis/log_structure_with_processnames_batch09_bus.json", batch=9)

#p.microvu()
#p.read_values()
#values = p.get_keyence_data()
#print(values)
