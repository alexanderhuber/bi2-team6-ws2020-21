import sqlite3
from process_YAML import Log
import os
import tqdm


class DatabaseManager:

    def __init__(self, db_file="database/database.db"):
        # Set to Assignment 1 Folder
        #self.path = os.path.dirname(os.getcwd())
        self.path = "/Users/amandahirschl/GIT Repos/bi2-team6-ws2020-21/Assignment 1"

        self.db_file = os.path.join(self.path, db_file)
        self.conn = self.create_connection()
        self.tables_file = os.path.join(self.path, "database/table_definitions")
        self.create_tables()

    def set_database(self, database_file):
        self.db_file = database_file

    def create_connection(self):
        conn = None
        try:
            conn = sqlite3.connect(self.db_file)
        except sqlite3.Error as e:
            print(e)
        return conn

    def create_tables(self):
        with open(self.tables_file, 'r') as stream:
            data = stream.read()
            sql = data.split(';')
        for command in sql:
            self.exec_sql(command)

    def insert_measurements(self, name, instance, value, type, log_order, timestamp, batch):
        sql = " INSERT INTO keyence_measurements VALUES ('{}', '{}', '{}', '{}', {}, '{}', '{}'); ".format(
            name, instance, value, type, log_order, timestamp, batch)
        print(sql)
        self.exec_sql(sql)

    def insert_microvu_measurements(self, name, instance, upright_value, lying_value, info, batch):
        sql = " INSERT INTO microvu_measurements VALUES ('{}', '{}', '{}', '{}', '{}', '{}'); ".format(
            name, instance, upright_value, lying_value, info, batch)
        print(sql)
        self.exec_sql(sql)

    def insert_index(self, filename, batch, id, name, process="", info=""):
        sql = " INSERT INTO files_index VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}'); ".format(
            filename, batch, id, name, process, info)
        print(sql)
        self.exec_sql(sql)


    def exec_sql(self,  sql):
        cur = self.conn.cursor()
        cur.execute(sql)
        self.conn.commit()
        return cur.lastrowid

    def insert_data_from_log(self, file):
        log_reader = Log()
        elements = log_reader.get_elements(file)
        sql = ""

        sql = sql + " CREATE TABLE IF NOT EXISTS global_log (concept_name text, cpee_name text, cpee_instance text ); "
        self.exec_sql(sql)

        sql = sql + " CREATE TABLE IF NOT EXISTS events (cpee_instance text, cpee_activity_uuid text, concept_name text" \
                    ", cpee_activity text, concept_instance text, lifecycle_transition text, timestamp text); "
        self.exec_sql(sql)

        for e in elements:
            if 'event' in e:
                event = e['event']
                concept_instance = event['concept:instance'] if 'concept:instance' in event else ''
                concept_name = event['concept:name'] if 'concept:name' in event else ''
                cpee_activity = event['cpee:activity'] if 'cpee:activity' in event else ''
                cpee_activity_uuid = event['cpee:activity_uuid'] if 'cpee:activity_uuid' in event else ''
                cpee_instance = event['cpee:instance'] if 'cpee:instance' in event else ''
                lifecycle_transition = event['lifecycle:transition'] if 'lifecycle:transition' in event else ''
                timestamp = event['time:timestamp'] if 'time:timestamp' in event else ''

                sql = " INSERT INTO events VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}'); ".format(
                    cpee_instance, cpee_activity_uuid, concept_name, cpee_activity, concept_instance, lifecycle_transition, timestamp)
                self.exec_sql(sql)

            elif 'log' in e:
                global_info = e['log']
                trace = global_info['trace'] if 'trace' in global_info else None
                concept_name = trace['concept:name'] if trace is not None else ''
                cpee_name = trace['cpee:name'] if trace is not None else ''
                cpee_instance = trace['cpee:instance'] if trace is not None else ''

                sql = " INSERT INTO global_log VALUES ('{}', '{}', '{}') ; ".format(concept_name, cpee_name,
                                                                                      cpee_instance)
                self.exec_sql(sql)

    def loop_all_files_in_folder(self, directory):
        files_count = len(os.listdir(directory))
        curr_count = 0
        for filename in os.listdir(directory):
            self.insert_data_from_log(directory+ '/' + filename)
            curr_count = curr_count+1
            print('{}/{} complete'.format(curr_count, files_count))

    def insert_file_index(self, id, number_id, process_name, timestamp=None, parent=None, batch=None):
        sql = " INSERT INTO index_table VALUES ('{}', '{}', '{}', '{}', '{}', '{}'); ".format(
            id, number_id, process_name, timestamp, parent, batch)
        #print(sql)
        self.exec_sql(sql)
