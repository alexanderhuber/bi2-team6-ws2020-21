import pandas as pd
import numpy as np
import warnings
from utils.data_preprocessor import DataPreprocessor


class CorrelationModel():
    
    def __init__(self, metric, correlation_threshold=0.95):
        metrics = ['mean', 'median']
        if not metric in metrics:
            self.metric = 'mean'
            warnings.warn(f"Metric must be one of {str(metrics)}. Using default metric 'mean'.")
        else:
            self.metric = metric
        if correlation_threshold < 0:
            correlation_threshold = 0
        if correlation_threshold > 1:
            correlation_threshold = 0.9
        self.correlation_threshold = correlation_threshold
        
    def fit_predict(self, df, error_threshold):
        anomaly_classification = self.__get_anomalies(df)
        input_data = self.__get_model_input(df, anomaly_classification)
        model = self.__get_model(input_data, error_threshold)
        drawing_model = self.__get_drawing_model(model, error_threshold)
        results = self.__get_results(input_data, model, drawing_model)
        return anomaly_classification, model, drawing_model, results

    def __get_anomalies(self, df):
        dp = DataPreprocessor(df)
        correlations = dp.get_correlations(df)
        df = df[['name', 'batch']].drop_duplicates()
        df.index = range(len(df))
        anomaly = correlations.loc[df['name']] < self.correlation_threshold
        df['anomaly'] = anomaly.values
        return df

    def __get_model_input(self, df, anomaly_classification):
        anomaly_parts = anomaly_classification[anomaly_classification['anomaly'] == True]['name']
        return df[~df['name'].isin(anomaly_parts)]
    
    def __get_drawing_model(self, model, error_threshold):
        # Load from file
        model_data = np.loadtxt('model.csv', delimiter=',')
        # Remove last element
        model_data = model_data[:-1]
        R = int(np.ceil(len(model_data) / len(model)))
        missing_count = len(model) * R - len(model_data)
        last_value = model_data[-1]
        append_values = [last_value for x in range(missing_count)]
        model_data = np.append(model_data, append_values)
        model_data = model_data.reshape(-1, R).mean(axis=1)
        location = np.linspace(0, 50, len(model_data))
        model_df = pd.DataFrame(zip(model_data, location), columns = ['value', 'location'])
        model_df['lower_error'] = model_df['value'] - error_threshold
        model_df['upper_error'] = model_df['value'] + error_threshold
        return model_df

    def __get_model(self, df, error_threshold):
        instance_data = df.groupby('name', sort=False)['value']
        instance_data = instance_data.apply(lambda x: pd.Series(x.values)).unstack()
        # Get reference
        if self.metric == 'mean':
            reference = instance_data.median(axis=0)
        if self.metric == 'median':
            reference = instance_data.median(axis=0)
        location = np.linspace(0, 50, len(reference.values))
        model = pd.DataFrame(zip(reference.values, location), columns=['value', 'location'])
        model['lower_error'] = model['value'] - error_threshold
        model['upper_error'] = model['value'] + error_threshold
        return model

    def __get_results(self, input_data, model, drawing_model):
        instance_data = input_data.groupby('name', sort=False)
        results = instance_data.apply(lambda x: self.__add_results(x, model, drawing_model))
        return results

    def __add_results(self, df, model, drawing_model):
        df['reference'] = model['value'].values
        df['drawing_reference'] = drawing_model['value'].values
        df['error'] = df['value'] - df['reference']
        df['drawing_error'] = df['value'] - df['drawing_reference']
        df['location'] = model['location'].values
        df['lower_error'] = model['lower_error'].values
        df['upper_error'] = model['upper_error'].values
        df['drawing_lower_error'] = drawing_model['lower_error'].values
        df['drawing_upper_error'] = drawing_model['upper_error'].values
        return df
        


    