import os

import yaml
import xes

# PyYAML and xes needed.
# In order to install:
# pip install pyyaml
# pip install xes

from anytree.importer import JsonImporter


def set_log_extensions(log):
    # log.add_default_extensions()
    # log.add_extension(xes.Extension(name="CPEE", prefix="cpee", uri=entry['log']['extension']['cpee']))
    # log.add_extension(xes.Extension(name="Micro", prefix="micro", uri='http://www.xes-standard.org/micro.xesext'))
    log.extensions = [
        xes.Extension(name="Concept", prefix="concept", uri="http://www.xes-standard.org/concept.xesext"),
        xes.Extension(name="Lifecycle", prefix="lifecycle", uri="http://www.xes-standard.org/lifecycle.xesext"),
        xes.Extension(name="Time", prefix="time", uri="http://www.xes-standard.org/time.xesext"),
        xes.Extension(name="Organizational", prefix="org", uri="http://www.xes-standard.org/org.xesext"),
        xes.Extension(name="CPEE", prefix="cpee", uri="http://cpee.org/cpee.xesext"),
        xes.Extension(name="Micro", prefix="micro", uri='http://www.xes-standard.org/micro.xesext')
    ]


def set_log_global_trace_attributes(log, entry):
    log.global_trace_attributes = [
        xes.Attribute(type="string", key="concept:name", value=entry['log']['global']['trace']['concept:name']),
        xes.Attribute(type="string", key="cpee:name", value=entry['log']['global']['trace']['cpee:name'])
    ]


def set_log_global_event_attributes(log, entry):
    return
    # log.global_event_attributes = [
    #     xes.Attribute(type="string", key='concept:instance', value=str(entry['log']['global']['event']['concept:instance'])),
    #     xes.Attribute(type="string", key='concept:name', value=entry['log']['global']['event']['concept:name']),
    #     # xes.Attribute(type="string", key='concept:endpoint', value=entry['log']['global']['event']['concept:endpoint']),
    #     xes.Attribute(type="string", key='id:id', value=entry['log']['global']['event']['id:id']),
    #     # xes.Attribute(type="string", key='cpee:uuid', value=entry['log']['global']['event']['cpee:uuid']),
    #     xes.Attribute(type="string", key='lifecycle:transition',
    #                   value=entry['log']['global']['event']['lifecycle:transition']),
    #     xes.Attribute(type="string", key='cpee:lifecycle:transition',
    #                   value=entry['log']['global']['event']['cpee:lifecycle:transition']),
    #     xes.Attribute(type="date", key='time:timestamp', value="1970-01-01T00:00:00.000+01:00")
    # ]


# def set_log_classifiers(log, entry):
#     log.classifiers = [
#         xes.Classifier(name="Event Name", keys="concept:name"),
#         xes.Classifier(name="Event ID", keys="id:id"),
#         xes.Classifier(name="Event ID Transition", keys="id:id lifecycle:transition"),
#         xes.Classifier(name="CPEE Classifier", keys="concept:name cpee:lifecycle:transition")
#     ]


def trace_set_attributes(trace, entry):
    trace.attributes = [
        xes.Attribute(type="string", key="concept:name", value=entry['log']['trace']['concept:name']),
        xes.Attribute(type="string", key="cpee:name", value=entry['log']['trace']['cpee:name']),
        xes.Attribute(type="string", key="cpee:instance", value=entry['log']['trace']['cpee:instance'])
    ]


def should_log_contain_event(entry):
    transition = entry['event']['cpee:lifecycle:transition']
    considered_transitions = ["activity/calling", "activity/done"]
    return transition in considered_transitions


def set_event_attributes(event, entry, process_level = 1, process_parent_id = None):
    event.attributes = [
        xes.Attribute(type="string", key="concept:instance", value=entry['event']['concept:instance']),
        xes.Attribute(type="string", key="concept:name", value=entry['event']['concept:name']),
        xes.Attribute(type="string", key="id:id", value=entry['event']['id:id']),
        xes.Attribute(type="string", key="cpee:instance", value=entry['event']['cpee:instance']),
        xes.Attribute(type="string", key="lifecycle:transition", value=entry['event']['lifecycle:transition']),
        xes.Attribute(type="string", key="cpee:lifecycle:transition", value=entry['event']['cpee:lifecycle:transition']),
        xes.Attribute(type="int", key='micro:level', value=str(process_level)),
        xes.Attribute(type="date", key="time:timestamp", value=entry['event']['time:timestamp'])
    ]
    if process_level > 1:
        event.add_attribute(xes.Attribute(type="id", key="micro:parentId", value=str(process_parent_id)))


def is_endpoint_available(entry):
    return 'concept:endpoint' in entry['event']


def is_subprocess(entry):
    transition = entry['event']['cpee:lifecycle:transition']
    return transition == 'task/instantiation'


def add_process_info(filename, process_level = 1, process_parent_id = None):
    if process_level > 2:
        return
    data = yaml.load_all(open(os.path.join(log_directory, filename + '.xes.yaml'), 'r'))
    for entry in data:
        if 'event' in entry and is_subprocess(entry):
            add_process_info(entry['event']['data']['data_receiver']['CPEE-INSTANCE-UUID'], process_level + 1, filename)
        if 'event' in entry and should_log_contain_event(entry):
            event = xes.Event()
            set_event_attributes(event, entry, process_level, process_parent_id)
            # if is_endpoint_available(entry):
            #    event.add_attribute(xes.Attribute(type="string", key="cpee:endpoint", value=entry['event']['concept:endpoint']))
            trace.add_event(event)

batches = ["batch07_bus", "batch08_bus", "batch09_bus"]

output_directory = os.path.normpath(
    "C:/Users/alexa/Nextcloud/Documents/WS2020-21/Business Intelligence 2/bi2-team6-ws2020-21/Assignment 1/xes")

log = xes.Log()
log.use_default_extensions = False
set_log_extensions(log)

log_properties_specified = False

for batch in batches:

    log_directory = os.path.join(os.path.normpath("C:/Users/alexa/Nextcloud/Documents/WS2020-21/Business Intelligence 2/Assignment 1/data/"), batch)
    structure = os.path.join(os.path.normpath("C:/Users/alexa/Nextcloud/Documents/WS2020-21/Business Intelligence 2/bi2-team6-ws2020-21/Assignment 1/loganalysis"), 'log_structure_' + batch + '.json')

    importer = JsonImporter()
    tree = importer.read(open(structure, 'r'))

    root = tree.children[0]
    root_file = root.name

    data = yaml.load_all(open(os.path.join(log_directory, root_file + '.xes.yaml'), 'r'))
    trace = xes.Trace()
    for entry in data:
        if 'log' in entry:
            if not log_properties_specified:
                set_log_global_trace_attributes(log, entry)
                set_log_global_event_attributes(log, entry)
                #set_log_classifiers(log, entry)
                log_properties_specified = True
            trace_set_attributes(trace, entry)
            break
    add_process_info(root_file, 1, None)
    log.add_trace(trace)

open(os.path.join(output_directory, "xes_log_with_subprocesses_" + "all_batches" + ".xes"), 'w').write(str(log))
print("Progress: Finished")


